export function generalFilters(value, type, enums) {
  if (value === undefined || value === null) {
    if (type) {
      const split = type.split(':');
      return split[0] === 'defaultValue' ? split[1] : value;
    }
    return value;
  }
  switch (type) {
    case 'array_to_string':
      if (value instanceof Array) {
        return value.toString();
      } else {
        return value;
      }
    case 'date':
      return dateFilter(value);
    case 'enums':
      return doEnums(enums, value);
    case 'time':
      return timeFilter(value);
  }

  if (value instanceof Object) {
    return value[type];
  }
  return value;
}

export function dateFilter(value) {
  return value ? formatDate(new Date(value * 1000), 'yyyy-MM-dd') : '';
}

export function timeFilter(value) {
  return value && `${ value }` !== `0`
    ? formatDate(new Date(value * 1000), 'yyyy-MM-dd hh:mm:ss')
    : '';
}

export function doEnums(e, v) {
  if (e instanceof Array) {
    const { name } = e.find(ev => `${ ev.value }` === `${ v }`) || {};
    if (name) {
      return name;
    }
  }
  return e[v];
}

export function formatDate(date, fmt) {
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
  }
  const o = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds(),
  };
  for (const k in o) {
    if (new RegExp(`(${ k })`).test(fmt)) {
      const str = o[k] + '';
      fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? str : padLeftZero(str));
    }
  }
  return fmt;
}

function padLeftZero(str) {
  return ('00' + str).substr(str.length);
}
