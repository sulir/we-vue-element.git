import Vue from 'vue'
import Router from 'vue-router'

import Grid1 from './views/grid/grid_1'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: {
        name: 'grid_1'
      }
    },
    {
      path: '/grid/grid_1',
      name: 'grid_1',
      component: Grid1
    }
  ]
})
