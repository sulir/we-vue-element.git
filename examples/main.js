import App from './App.vue';
import router from './router';
import Vue from 'vue';
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import weElement from '../src';
import { generalFilters } from './filters';

const grid1 = require(`./mock/grid1.json`);
const mockData = { grid1 };
Vue.use(Element);
Vue.use(weElement, {
  WeGrid: {
    globalOptions: {
      i18n: (key, value) => {
        console.log(key, value);
        return key
      },
      generalFilters,
      remote: true,
      i18nPrefix: 'content',
      // eslint-disable-next-line no-unused-vars
      remoteMethod: ({ queryParams, me, searchRole, gridUrl }) => {
        return Promise.resolve({ data: mockData[`${ gridUrl }`] });
      },
    },
    filters: { generalFilters },
  },
});
Vue.config.productionTip = false;
new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
