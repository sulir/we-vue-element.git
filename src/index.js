import WeGrid from '../packages/grid';

const components = [WeGrid];

const install = function(Vue, options = {}) {
  if (!install.installed) {
    components.map(component => {
      Vue.use(component, options[component.name]);
    });
  }
};

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}

// eslint-disable-next-line import/export
export * from '../packages/grid';
export default {
  install,
  WeGrid,
};
