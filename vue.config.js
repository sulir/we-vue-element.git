const path = require('path')

function resolve (dir) {
  return path.join(__dirname, '.', dir)
}

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/we-vue-element/' : '/',
  outputDir: 'docs',
  assetsDir: 'static',
  productionSourceMap: false,
  configureWebpack: {
    performance: {
      hints: false
    }
  },
  pages: {
    index: {
      entry: 'examples/main.js',
      template: 'public/index.html',
      filename: 'index.html',
      title: 'We element grid component for Vue.'
    }
  },
  chainWebpack (config) {
    config.resolve.alias
      .set('@', resolve('examples'))
    config.output
      .set('libraryExport', 'default')
      .set('library', 'VueElementWe')
    if (process.env.npm_lifecycle_event.indexOf('lib') === 0) {
      let LJC = {
        root: 'LJC',
        commonjs: 'lc-js-components',
        commonjs2: 'lc-js-components',
        amd: 'lc-js-components'
      }
      if (config.has('externals')) {
        config.externals
          .set('lc-js-components', LJC)
      } else {
        config
          .set('externals', {
            'lc-js-components': LJC
          })
      }
    }
  }
}
