# we-vue-element

#### Description

Expansion based on vue-element

#### Installation

```shell
npm install we-vue-element
```

#### Show demo
```.shell
git clone https://gitee.com/sulir/we-vue-element.git

npm install

npm serve

open http://localhost:8080/
```
#### Basic Usage


1.  grid(table)
```html
<we-grid ref="weGrid"
    :items="items: [
        { key: 'orderId' },
        { key: 'paymentId' },
        { key: 'amount' },
        { key: 'userName' },
        { key: 'phone' },
        { key: 'subject' },
        { key: 'createAt', filter: 'time' },
        { width: '120px', key: 'paymentClass', 
          filter: 'enums', 
          enums: [ { value: 1, name: 'qrCode' },
            { value: 2, name: 'transfer' },
            { value: 3, name: 'recharge' },
            { value: 4, name: 'cashout' } 
          ]
        }, 
        { width: '80px', key: 'status', display: 'Status', filter: 'enums',
          enums: paymentStatus: [
            { value: 0, name: 'fail' },
            { value: 1, name: 'success' }
          ]
        }
    ]"
    :operationButtons="[{
                icon: 'el-icon-edit',
                title: '编辑',
                handle: (index, row) => {
                  alert(`index:${ index }`);
                  alert(`row:${ JSON.stringify(row) }`);
                }
              }
     ]"
    :query="{ orderId: '1209950692047876096'}"
    grid-url="grid1" 
    @change="()=>{console.log('change event.')}"
    @onBeforeSearch="()=>{console.log('before search event.')}"
>
    <template slot="status" slot-scope="scope">
        <table-status-icon :colorEnum="{ 1: 'approved',  2: 'rejected' }"
        :status="scope.row.status && scope.row.status"
        :statusEnum="{ 1: 'enable', 2: 'disable' }" />
    </template>
</we-grid>
```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
