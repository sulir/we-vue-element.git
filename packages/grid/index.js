
/*
* we-vue-element index.js
* Author: sulirlin
* Gitee: https://gitee.com/sulir/we-vue-element
*/

import WeGrid from '../grid/src/grid'

WeGrid.install = (Vue,options) => {
  if (options) {
    const { globalOptions, filters } = options;
    WeGrid.props.globalOptions.default = () => globalOptions;
    WeGrid.filters = filters;
  }
  Vue.component(WeGrid.name, WeGrid)
}
export default WeGrid;
