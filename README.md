# we-vue-element

#### 介绍
基于vue-element的拓展

#### 安装教程使用

```shell
npm install we-vue-element
```

#### 本地运行事例

```.shell
git clone https://gitee.com/sulir/we-vue-element.git

npm install

npm serve

open http://localhost:8080/

```

#### 使用说明

1.  列表

```html
<we-grid ref="weGrid"
    :items="items: [
        { key: 'orderId' },
        { key: 'paymentId' },
        { key: 'amount' },
        { key: 'userName' },
        { key: 'phone' },
        { key: 'subject' },
        { key: 'createAt', filter: 'time' },
        { width: '120px', key: 'paymentClass', 
          filter: 'enums', 
          enums: [ { value: 1, name: 'qrCode' },
            { value: 2, name: 'transfer' },
            { value: 3, name: 'recharge' },
            { value: 4, name: 'cashout' } 
          ]
        }, 
        { width: '80px', key: 'status', display: '状态', filter: 'enums',
          enums: paymentStatus: [
            { value: 0, name: 'fail' },
            { value: 1, name: 'success' }
          ]
        }
    ]"
    :operationButtons="[{
                icon: 'el-icon-edit',
                title: '编辑',
                handle: (index, row) => {
                  alert(`index:${ index }`);
                  alert(`row:${ JSON.stringify(row) }`);
                }
              }
     ]"
    :query="{ orderId: '1209950692047876096'}"
    grid-url="grid1" 
    @change="()=>{console.log('change event.')}"
    @onBeforeSearch="()=>{console.log('before search event.')}"
>
    <template slot="status" slot-scope="scope">
        <table-status-icon :colorEnum="{ 1: 'approved',  2: 'rejected' }"
        :status="scope.row.status && scope.row.status"
        :statusEnum="{ 1: 'enable', 2: 'disable' }" />
    </template>
</we-grid>
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
