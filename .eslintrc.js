module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/essential", "standard", "prettier"],
  rules: {
    "no-console": "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "template-curly-spacing": ["error", "always"],
    "no-unused-vars": [
      "error",
      { vars: "all", args: "after-used", ignoreRestSiblings: false }
    ]
  },
  parserOptions: {
    parser: "babel-eslint"
  }
};
